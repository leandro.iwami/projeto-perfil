package br.com.itau.investimento.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import br.com.itau.investimento.models.Termo;
import br.com.itau.investimento.repositories.TermoRepository;

@Service
public class TermoService {
	@Autowired
	TermoRepository termoRepository;
	
	public void inserir(Termo termo) {
		termoRepository.save(termo);		
	}
	public Iterable<Termo> obterTermo(){
		return termoRepository.findAll();
	}
	
	public Optional<Termo> obterTermoPorId(int id) {
		return termoRepository.findById(id);
	}
	
	
}
