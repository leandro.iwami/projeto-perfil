package br.com.itau.investimento.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.investimento.models.Cliente;
import br.com.itau.investimento.models.ClienteTermo;
import br.com.itau.investimento.models.Termo;
import br.com.itau.investimento.repositories.ClienteTermoRepository;


@Service
public class ClienteTermoService {
	@Autowired
	ClienteTermoRepository clienteTermoRepository;
	
	@Autowired
	ClienteService clienteService;
	
	public void inserir(ClienteTermo clienteTermo) {
		clienteTermoRepository.save(clienteTermo);		
	}
	
	public Iterable<ClienteTermo> obterClienteTermo(){
		return clienteTermoRepository.findAll();
	}
	
	public Optional<ClienteTermo> obterClienteTermoPorId(int id) {
		return clienteTermoRepository.findById(id);
	}
	
	public boolean buscarClienteTermo(int idCliente, int idTermo) {

		List<ClienteTermo> listaSaida = new ArrayList<ClienteTermo>();
		
		Cliente cliente = clienteService.obterClientePorId(idCliente);

		Iterable<ClienteTermo> listaTermo = clienteTermoRepository.findAllByCliente(cliente);


		for (ClienteTermo clienteTermo : listaTermo) {
			Termo termo = clienteTermo.getTermo();
			
			if (termo.getIdTermo() == idTermo) {
			return true;
			}
		}
		return false;
	}
}
