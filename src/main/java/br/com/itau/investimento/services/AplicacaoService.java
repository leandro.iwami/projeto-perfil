package br.com.itau.investimento.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.investimento.models.Aplicacao;
import br.com.itau.investimento.models.AplicacaoSaida;
import br.com.itau.investimento.models.Cliente;
import br.com.itau.investimento.models.ClienteTermo;
import br.com.itau.investimento.models.PerfilInvestidor;
import br.com.itau.investimento.models.Produto;
import br.com.itau.investimento.repositories.AplicacaoRepository;

@Service
public class AplicacaoService {
	
	@Autowired
	AplicacaoRepository aplicacaoRepository;
	
	@Autowired
	ClienteService clienteService;
	
	@Autowired
	CatalogoService catalogoService;
	
	@Autowired
	CarteiraInvestimentoService carteiraInvestimentoService;
	
	@Autowired
	ClienteTermoService clienteTermoService;
	
	@Autowired
	PerfilInvestidorService perfilInvestidorService;
	
	
	public Iterable<AplicacaoSaida> listarAplicacoes(int id){
		
		List<AplicacaoSaida> listasaida = new ArrayList<AplicacaoSaida>();
		
		Cliente cliente = clienteService.obterClientePorId(id);

		Iterable<Aplicacao> listaaplicacao = aplicacaoRepository.findAllByCliente(cliente);
		
		for (Aplicacao aplicacao : listaaplicacao) {
			AplicacaoSaida aplicacaoSaida = new AplicacaoSaida();
			aplicacaoSaida.setIdAplicacao(aplicacao.getIdAplicacao());
			aplicacaoSaida.setMes(aplicacao.getMeses());
			Produto produto = aplicacao.getProduto();
			Optional<Produto> produtoOptional = catalogoService.obterProdutoPorId(produto.getIdProduto());
			aplicacaoSaida.setProduto(produtoOptional.get());
			aplicacaoSaida.setValor(aplicacao.getValor());
			listasaida.add(aplicacaoSaida);
			
		}
		return listasaida;
	}
	
	public int calcular(Aplicacao aplicacao){
//		public List<Simulacao> calcular(Aplicacao aplicacao, int idCliente){
		
		Cliente cliente = aplicacao.getCliente();
		
		Optional<PerfilInvestidor> perfilInvestidorOptional = perfilInvestidorService.consultarPerfilInvestidor(cliente.getIdCliente());
		
		if (!perfilInvestidorOptional.isPresent()) {
			return verificarTCQCliente(cliente.getIdCliente(), aplicacao );
		 }
			
		Produto produto = aplicacao.getProduto();
		Optional<Produto> produtoOptional = catalogoService.obterProdutoPorId(produto.getIdProduto());
		
		if(!produtoOptional.isPresent()) {
			return 3;
		}
		
		produto = produtoOptional.get();
		aplicacao.setProduto(produto);
		
		aplicacao.setCliente(clienteService.obterClientePorId(cliente.getIdCliente()));

		if(carteiraInvestimentoService.verificarEnquadramento(aplicacao.getValor(), cliente.getIdCliente(), produto.getRiscoProduto())) {
			aplicacaoRepository.save(aplicacao);
			return 0;
		}else{
			return 2;
		}
		
//		return calcularPorProduto(produtoOptional.get(), aplicacao.getValor(), aplicacao.getMeses());
	}
	
	private int verificarTCQCliente(int idCliente, Aplicacao aplicacao) {
		int termo = 1;
		if(!clienteTermoService.buscarClienteTermo(idCliente,termo )) {
			return 1;
		}else {
			aplicacaoRepository.save(aplicacao);
			return 0;
		}	 
	}
}
