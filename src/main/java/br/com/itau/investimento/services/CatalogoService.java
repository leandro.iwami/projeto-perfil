package br.com.itau.investimento.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import br.com.itau.investimento.models.Cliente;
import br.com.itau.investimento.models.Produto;
import br.com.itau.investimento.repositories.ProdutoRepository;

@Service
public class CatalogoService {
	@Autowired
	ProdutoRepository produtoRepository;
	
	public Iterable<Produto> obterProdutos(){
		return produtoRepository.findAll();
	}
	
	public Optional<Produto> obterProdutoPorId(int id) {
		return produtoRepository.findById(id);
	}

	public void inserir(Produto produto) {
		produtoRepository.save(produto);
		
	}
	public boolean atualizar(int id, Produto produto) {
		Optional<Produto> produtoOptional = produtoRepository.findById(id);

		if (produtoOptional.isPresent()) {
			produtoRepository.save(produto);
			return true;
		}

		return false;
	}
	
	public boolean remover(int id) {
		Optional<Produto> produtoOptional = produtoRepository.findById(id);

		if (produtoOptional.isPresent()) {
			produtoRepository.delete(produtoOptional.get());
			return true;
		}

		return false;
	}
}
