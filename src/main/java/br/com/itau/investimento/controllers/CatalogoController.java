package br.com.itau.investimento.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.investimento.models.Cliente;
import br.com.itau.investimento.models.ClienteSaida;
import br.com.itau.investimento.models.Produto;
import br.com.itau.investimento.services.CatalogoService;
import br.com.itau.investimento.services.ClienteService;

@RestController
@RequestMapping("/produto")
public class CatalogoController {
	
	@Autowired
	CatalogoService catalogoService;
	
	@GetMapping
	public Iterable<Produto> buscarProduto(){
		return catalogoService.obterProdutos();
	}
	
	@GetMapping("/{id}")
	public Optional<Produto> buscarProdutoId(@PathVariable int id){
		return catalogoService.obterProdutoPorId(id);
	}
	
	@PostMapping("/inserir")
	public void inserirProduto(@RequestBody Produto produto) {
		catalogoService.inserir(produto);
	}
	@PatchMapping("/{id}")
	public ResponseEntity atualizarProduto(@PathVariable int id, @RequestBody Produto produto) {
		boolean resultado = catalogoService.atualizar(id, produto);

		if (!resultado) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(produto);
	}

}
