package br.com.itau.investimento.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.investimento.models.Cliente;
import br.com.itau.investimento.models.PerfilInvestidor;
import br.com.itau.investimento.services.PerfilInvestidorService;

@RestController
@RequestMapping("/perfilinvestidor")
public class PerfilInvestidorController {
	
	@Autowired
	PerfilInvestidorService perfilInvestidorService;
	
	@GetMapping("/{idCliente}")
	public Optional<PerfilInvestidor> obterPerfilInvestidor(@PathVariable Integer idCliente) {
		return perfilInvestidorService.consultarPerfilInvestidor(idCliente);
	}
	
	@PutMapping("/{idCliente}")
	public PerfilInvestidor alterarPerfilInvestidor(@PathVariable Integer idCliente, @RequestBody PerfilInvestidor perfil) {
		return perfilInvestidorService.alterarPerfil(idCliente, perfil.getCodigoPerfil());
	}

}
