package br.com.itau.investimento.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.investimento.models.Aplicacao;
import br.com.itau.investimento.models.ClienteTermo;

import br.com.itau.investimento.models.Termo;

import br.com.itau.investimento.services.ClienteTermoService;
import br.com.itau.investimento.services.TermoService;

@RestController
@RequestMapping("/termo")
public class ClienteTermoController {
	@Autowired
	TermoService termoService;
	
	@Autowired
	ClienteTermoService clienteTermoService;
	
	@GetMapping
	public Iterable<ClienteTermo> buscarClienteTermo(){
		return clienteTermoService.obterClienteTermo();
	}
	
	@PostMapping("/inserirtermo")
	public void inserirTermo(@RequestBody Termo termo) {
		termoService.inserir(termo);
	}
	@PostMapping("/inserirclientetermo")
	public void inserirClienteTermo(@RequestBody ClienteTermo clientetermo) {
		clienteTermoService.inserir(clientetermo);
	}
	
//	@GetMapping("/{id}/{idT}/teste")
//	public Iterable<ClienteTermo> listarTermos(@PathVariable int id, @PathVariable int idT){
//		return clienteTermoService.buscarClienteTermo(id, idT);
//	}
}
