package br.com.itau.investimento.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.investimento.models.ListaPerguntas;
import br.com.itau.investimento.models.ListaRespostas;
import br.com.itau.investimento.models.Mensagem;
import br.com.itau.investimento.models.Questionario;
import br.com.itau.investimento.services.QuestionarioService;

@RestController
@RequestMapping("/questionario")
@CrossOrigin
public class QuestionarioController {

	@Autowired
	QuestionarioService questionarioService;

	@GetMapping
	public Iterable<Questionario> listarQuestionarios() {
		return questionarioService.buscarTodasPerguntas();
	}

	@PostMapping("/inserir")
	public void inserir(@RequestBody ListaPerguntas listaPerguntas) {
		questionarioService.inserirPergunta(listaPerguntas);
	}

	@PutMapping("/{id}/atualizar")
	public void atualizarPergunta(@PathVariable int id, @RequestBody Questionario questionario) {
		questionarioService.atualizarPergunta(id, questionario);

	}

	@PostMapping("/{id}/remover")
	public void removerPergunta(@PathVariable int id) {
		questionarioService.removerPergunta(id);

	}

	@PostMapping("/{id}/enviarResposta")
	public Mensagem calculo(@PathVariable int id, @RequestBody ListaRespostas listaRespostas) {
		
		 return questionarioService.calcularRespostas(id, listaRespostas);
		 
	}
}
