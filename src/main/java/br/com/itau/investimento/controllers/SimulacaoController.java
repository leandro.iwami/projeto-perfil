package br.com.itau.investimento.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.investimento.models.Aplicacao;
import br.com.itau.investimento.services.SimulacaoService;

@RestController
@RequestMapping("/simulacao")
public class SimulacaoController {

	@Autowired
	SimulacaoService simulacaoService;
	
//	@PostMapping("/{idCliente}/simular")
//	public int simular( @PathVariable int idCliente, @RequestBody Aplicacao aplicacao){
//		return simulacaoService.calcularPorProduto(aplicacao, idCliente);
//	}
}
