package br.com.itau.investimento.filters;

import java.io.IOException;
import java.util.Collections;
import java.util.Optional;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import br.com.itau.investimento.helpers.JwtHelper;

public class AuthorizationFilter extends HttpFilter {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		String token = obterToken(request);
		
		Optional<Integer> idUsuarioOptional = JwtHelper.verificar(token);
		
		if(idUsuarioOptional.isPresent()) {		
			Authentication autenticacao = new UsernamePasswordAuthenticationToken(
					idUsuarioOptional.get(), 
					null, 
					Collections.emptyList()); 
			
			SecurityContextHolder.getContext().setAuthentication(autenticacao);
		}

		super.doFilter(request, response, chain);
	}
	
	private String obterToken(HttpServletRequest request) {
		String token = request.getHeader("Authorization");
		
		if(token == null) {
			return "";
		}
		
		token = token.replace("Bearer", "");
		token = token.trim();
		
		return token;
	}
}
