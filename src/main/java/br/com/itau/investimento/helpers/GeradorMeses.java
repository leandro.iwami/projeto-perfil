package br.com.itau.investimento.helpers;

import java.time.Month;

public class GeradorMeses {
	private int mesAtual = 0;
	
	public Month obterProximoMes() {
		Month[] meses = Month.values();
		
		if(mesAtual == 12) {
			mesAtual = 0;
		}
		
		Month mes = meses[mesAtual];
		
		mesAtual++;
		
		return mes;
	}
	
}
