package br.com.itau.investimento.configurations;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import br.com.itau.investimento.filters.AuthorizationFilter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
//        httpSecurity.authorizeRequests().antMatchers("/").permitAll().and()
//                .authorizeRequests().antMatchers("/console/**").permitAll();
// 
//        httpSecurity.csrf().disable();
//        httpSecurity.headers().frameOptions().disable();
		
		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		
		http
			.csrf().disable()
			.authorizeRequests()
			.antMatchers(HttpMethod.POST, "/cliente/inserir").permitAll()
			.antMatchers(HttpMethod.POST, "/login").permitAll()
			.antMatchers(HttpMethod.POST, "/produto/inserir").permitAll()
			.antMatchers(HttpMethod.PATCH, "/produto/{id}").permitAll()
			.antMatchers(HttpMethod.GET, "/termo").permitAll()
			.antMatchers(HttpMethod.POST, "/termo/inserirtermo").permitAll()
			.antMatchers(HttpMethod.GET, "/questionario").permitAll()
			.antMatchers(HttpMethod.POST, "/questionario/1/enviarResposta").permitAll()
			.antMatchers(HttpMethod.OPTIONS, "/questionario/1/enviarResposta").permitAll()
			.antMatchers(HttpMethod.POST, "/questionario/inserir").permitAll()
			.antMatchers(HttpMethod.POST, "/{id}/enviarResposta").permitAll()
			.anyRequest().authenticated()
			.and()
			.addFilterBefore(new AuthorizationFilter(), UsernamePasswordAuthenticationFilter.class);

    }
}
