package br.com.itau.investimento.models;

import java.util.List;

public class AplicacaoSaida {
	
	private int idAplicacao;
	private Produto produto;
	private double valor;
	private double mes;
	
	public int getIdAplicacao() {
		return idAplicacao;
	}
	public void setIdAplicacao(int idAplicacao) {
		this.idAplicacao = idAplicacao;
	}
	public Produto getProduto() {
		return produto;
	}
	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	public double getMes() {
		return mes;
	}
	public void setMes(double mes) {
		this.mes = mes;
	}
	
	

}
