package br.com.itau.investimento.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@Entity
public class Aplicacao {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idAplicacao;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name="id_cliente")
	private Cliente cliente;

	@NotNull
	@ManyToOne
	@JoinColumn(name="id_produto")
	private Produto produto;
	
	private double valor;
	private String email;
	private int meses;
	
	public int getId() {
		return idAplicacao;
	}
	public void setId(int id) {
		this.idAplicacao = id;
	}

	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getMeses() {
		return meses;
	}
	public void setMeses(int meses) {
		this.meses = meses;
	}
	public int getIdAplicacao() {
		return idAplicacao;
	}
	public void setIdAplicacao(int idAplicacao) {
		this.idAplicacao = idAplicacao;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public Produto getProduto() {
		return produto;
	}
	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	
}
