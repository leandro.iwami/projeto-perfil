package br.com.itau.investimento.models;

import java.util.List;

public class CarteiraInvestimento {
	
	private int idCliente;
	private List<Aplicacao> aplicacao;
	
	
	public List<Aplicacao> getAplicacao() {
		return aplicacao;
	}
	public void setAplicacao(List<Aplicacao> aplicacao) {
		this.aplicacao = aplicacao;
	}
	public int getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}
	
	

}
