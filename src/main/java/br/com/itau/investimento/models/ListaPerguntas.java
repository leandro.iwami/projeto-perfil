package br.com.itau.investimento.models;

import java.util.ArrayList;
import java.util.List;

public class ListaPerguntas {

	private List<Questionario> listaPerguntas = new ArrayList<Questionario>();

	public List<Questionario> getListaPerguntas() {
		return listaPerguntas;
	}

	public void setListaPerguntas(List<Questionario> listaPerguntas) {
		this.listaPerguntas = listaPerguntas;
	}

}
