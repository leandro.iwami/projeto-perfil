package br.com.itau.investimento.models;

public class Mensagem {
	String mensagem;

	public String getMensagem() {
		return mensagem;
	}

	public String setMensagem(String mensagem) {
		return this.mensagem = mensagem;
	}

}
