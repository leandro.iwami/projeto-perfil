package br.com.itau.investimento.models;

public class Resposta {
	private int resposta;

	public int getResposta() {
		return resposta;
	}

	public void setResposta(int resposta) {
		this.resposta = resposta;
	}

}
