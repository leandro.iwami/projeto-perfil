package br.com.itau.investimento.repositories;

import org.springframework.data.repository.CrudRepository;
import br.com.itau.investimento.models.Cliente;
import br.com.itau.investimento.models.PerfilInvestidor;

public interface PerfilInvestidorRepository extends CrudRepository<PerfilInvestidor, Integer>{

}
