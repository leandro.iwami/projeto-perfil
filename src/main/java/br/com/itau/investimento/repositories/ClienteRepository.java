package br.com.itau.investimento.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import br.com.itau.investimento.models.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Integer> {
	public Optional<Cliente> findByCpf(String Cpf);
}
