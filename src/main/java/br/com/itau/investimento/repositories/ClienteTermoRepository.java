package br.com.itau.investimento.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.itau.investimento.models.Cliente;
import br.com.itau.investimento.models.ClienteTermo;

public interface ClienteTermoRepository extends JpaRepository<ClienteTermo, Integer> {
	public Iterable<ClienteTermo> findAllByCliente(Cliente cliente);
}
