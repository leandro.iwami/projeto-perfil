
package br.com.itau.investimento.services;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.com.itau.investimento.models.ListaPerguntas;
import br.com.itau.investimento.models.Questionario;
import br.com.itau.investimento.repositories.QuestionarioRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(QuestionarioService.class)
public class QuestionarioServiceTest {

	@Autowired
	QuestionarioService questionarioService;

	@MockBean
	QuestionarioRepository questionarioRepository;

	@MockBean
	PerfilInvestidorService perfilInvestidorService;

	@Autowired
	MockMvc mockMvc;

	ArrayList<Questionario> listaPerguntas;
	Questionario questionario1;
	Questionario questionario2;
	Questionario questionario;
	ListaPerguntas listaPerguntasClasse;

	@Before
	public void inicializar() {
		listaPerguntas = new ArrayList<>();
		questionario1 = new Questionario();
		listaPerguntasClasse = new ListaPerguntas();

		questionario1.setIdPergunta(1);
		questionario1.setPergunta("pergunta 1");
		listaPerguntas.add(questionario1);

		questionario2 = new Questionario();
		questionario2.setIdPergunta(2);
		questionario2.setPergunta("pergunta 2");
		listaPerguntas.add(questionario2);
		listaPerguntasClasse.setListaPerguntas(listaPerguntas);

	}

	@Test
    public void testarInsercaoPergunta() {
        questionarioService.inserirPergunta(listaPerguntasClasse);
        for (int i = 0; i < listaPerguntas.size(); i++) {

            assertEquals(true, true);
        }
    }
}

